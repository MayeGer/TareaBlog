package com.ucbcba.blog.controllers;

import com.ucbcba.blog.entities.Category;
import com.ucbcba.blog.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class CategoryController {


    private CategoryService categoryService;
    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @RequestMapping(value = "/category/new", method = RequestMethod.GET)
    public String newCategory(Model model) {
        model.addAttribute("category", new Category());
        return "categoryForm";
    }

    @RequestMapping(value = "/category", method = RequestMethod.POST)
    public String save(@Valid Category category, BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            return "categoryForm";
        }
        categoryService.saveCategory(category);
        return "redirect:/categories";
    }

    @RequestMapping(value = "/category/{id}", method = RequestMethod.GET)
    public String showCategory(@PathVariable Integer id, Model model) {

        Category category = categoryService.getCategoryById(id);
        model.addAttribute("category", category);
        return "category";
    }

    @RequestMapping(value = "/category/editar/{id}", method = RequestMethod.GET)
    public String editCategory(@PathVariable Integer id, Model model) {
        model.addAttribute("category", categoryService.getCategoryById(id));
        return "categoryForm";
    }

    @RequestMapping(value = "/category/eliminar/{id}", method = RequestMethod.GET)
    public String deleteCategory(@PathVariable Integer id, Model model) {
        categoryService.deleteCategory(id);
        return "redirect:/categories";
    }

    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("categories", categoryService.listAllCategories());
        return "categories";
    }


}
