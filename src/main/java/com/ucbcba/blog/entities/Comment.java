package com.ucbcba.blog.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.util.Calendar;

/**
 * Created by amolina on 26/09/17.
 */
@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Size(min = 1,max = 50,message = "Valido entre 1 y 50 caracteres")
    private String text;

    @NotNull
    @ManyToOne
    @JoinColumn(name="post_id")
    private Post post;

    @NotNull
    private Integer likes = 0 ;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    private java.sql.Date date=new Date(Calendar.getInstance().getTime().getTime());///verificar

    public java.sql.Date getDate() {
        return date;
    }

    public void setDate(java.sql.Date date) {
        this.date = date;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Comment(){

    }
    public Comment(Post post){
        this.post = post;

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
