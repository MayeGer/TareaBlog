package com.ucbcba.blog.controllers;

import com.ucbcba.blog.entities.Comment;
import com.ucbcba.blog.entities.Post;
import com.ucbcba.blog.services.CategoryService;
import com.ucbcba.blog.services.CommentService;
import com.ucbcba.blog.services.PostService;
import com.ucbcba.blog.services.UserService;
import javafx.geometry.Pos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * Created by amolina on 19/09/17.
 */
@Controller
public class PostController {

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    String hello() {
        return "index";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String hello(Model model) {
        model.addAttribute("posts", postService.listAllPosts());
        return "home";
    }
    private CommentService commentService;

    private PostService postService;

    private CategoryService categoryService;

    private UserService userService;
    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Autowired
    public void setPostService(PostService postService) {
        this.postService = postService;
    }

    @RequestMapping(value = "/post/new", method = RequestMethod.GET)
    public String newPost(Model model) {
        model.addAttribute("post", new Post());
        model.addAttribute("categories",categoryService.listAllCategories());
        model.addAttribute("users",userService.listAllUsers());
        model.addAttribute("editando", false);
        model.addAttribute("pagina", false);
        return "postForm";
    }

    @RequestMapping(value = "/post", method = RequestMethod.POST)
    public String save(@Valid Post post, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()){
            model.addAttribute("categories",categoryService.listAllCategories());
            model.addAttribute("users",userService.listAllUsers());
            return "postForm";
        }
        postService.savePost(post);
        return "redirect:/posts";
    }

    @RequestMapping(value = "/post/{id}", method = RequestMethod.GET)
    public String showPost(@PathVariable Integer id, Model model) {

        Post post = postService.getPostById(id);
        model.addAttribute("post", post);
        model.addAttribute("comment", new Comment(post));
        return "post";
    }

    @RequestMapping(value = "/post/editar/{id}", method = RequestMethod.GET)
    public String editPost(@PathVariable Integer id, Model model) {
        model.addAttribute("post", postService.getPostById(id));
        model.addAttribute("editando", true);
        model.addAttribute("pagina", true);
        return "postForm";
    }

    /**LIKES**/
    @RequestMapping(value = "/post/like/{id}", method = RequestMethod.GET)
    public String like(@PathVariable Integer id, Model model) {
        Post post=postService.getPostById(id);
        if(post.getLikes()>=0){
            post.setLikes(post.getLikes()+1);
        }
        postService.savePost(post);
        return "redirect:/post/"+id;
    }

    /**DISLIKES**/
    @RequestMapping(value = "/post/dislike/{id}", method = RequestMethod.GET)
    public String dislike(@PathVariable Integer id, Model model) {
        Post post=postService.getPostById(id);
        if(post.getLikes()>0){
            post.setLikes(post.getLikes()-1);
        }
        postService.savePost(post);
        return "redirect:/post/"+id;
    }

    @RequestMapping(value = "/post/eliminar/{id}", method = RequestMethod.GET)
    public String deletePost(@PathVariable Integer id, Model model) {

        for (Comment comment:commentService.listAllComments()) {
            if(comment.getPost().getId()==id){
                commentService.deleteComment(comment.getId());
            }
        }
        postService.deletePost(id);
        return "redirect:/posts";
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("posts", postService.listAllPosts());
        return "posts";
    }

}
