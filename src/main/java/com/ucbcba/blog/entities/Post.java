package com.ucbcba.blog.entities;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

/**
 * Created by amolina on 19/09/17.
 */
@Entity
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Size(min = 1,max = 20,message = "Valido entre 1 y 20 caracteres")
    private String title;

    @NotNull
    @Size(min = 1,max = 100,message = "Valido entre 1 y 100 caracteres")
    private String text;


    @OneToMany(mappedBy = "post", cascade = CascadeType.ALL)
    private Set<Comment> comments;

    @NotNull
    private Integer likes = 0 ;

    private String image;

    private String pagi;

    @NotNull
    @ManyToOne
    @JoinColumn(name="category_id")
    private Category category;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    private java.sql.Date date;

    public java.sql.Date getDate() {
        return date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
       this.image = image;
    }

    public void setDate(java.sql.Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public String getPagi() {
        return pagi;
    }

    public void setPagi(String pagi) {
        this.pagi = pagi;
    }
}
