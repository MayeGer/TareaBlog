insert into user(name) values("usuario 1");
insert into user(name) values("usuario 2");
insert into category(name) values("categoria 1");
insert into category(name) values("categoria 2");
insert into post(date,likes,text,title,category_id,user_id) values("2017-10-12",2,"soy post 1","post1",1,1);
insert into post(date,likes,text,title,category_id,user_id) values("2017-10-12",2,"soy post 2","post1",2,2);
insert into comment(text,post_id,likes) values("primer comentario en post 1",1,1);
insert into comment(text,post_id,likes) values("primer comentario en post 2",2,1);