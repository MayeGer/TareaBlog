package com.ucbcba.blog.repositories;

import com.ucbcba.blog.entities.Category;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface CategoryRepository extends CrudRepository<Category,Integer>{
}
